public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("JBR2.10");
        Author author1 = new Author();
        Author author2 = new Author("Lê Nguyễn Đức Thạnh ","thanhlnd@devcamp.edu.vn",'m');
        Book book1 = new Book();
        Book book2 = new Book("Thạnh",author2,200000,25);

        System.out.println("Author 1 : ");
        System.out.println(author1.toString());
        System.out.println(book1.toString());

        System.out.println("Author 2 : ");
        System.out.println(author2.toString());
        System.out.println(book2.toString());

    }
}
